ThisBuild / scalaVersion := "2.12.8"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "parking"
ThisBuild / organizationName := "Parking Lot"

lazy val root = (project in file("."))
  .settings(
    name := "parking",
    libraryDependencies += "org.typelevel"     %% "cats-effect"  % "1.2.0",
    libraryDependencies += "io.monix"          %% "monix"        % "3.0.0-RC2",
    libraryDependencies += "com.github.gvolpe" %% "console4cats" % "0.6.0",
    libraryDependencies += "org.scalatest"     %% "scalatest"    % "3.0.5" % Test,
    scalacOptions += "-language:higherKinds",
  )
