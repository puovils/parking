package parking

import org.scalatest._
import parking.core._

class ParkingLotSpec extends FunSuite with Matchers {
  import VehicleType._

  val emptyParkingLot = ParkingLot.empty(bottom = -1, top = 2, floorCapacity = 3)

  test("create empty") {
    emptyParkingLot shouldEqual ParkingLot(
      bottom = -1,
      top = 2,
      floors = Map(
        -1 -> Floor.empty(3),
        0  -> Floor.empty(3),
        1  -> Floor.empty(3),
        2  -> Floor.empty(3),
      )
    )
  }

  test("take spot") {
    emptyParkingLot.takeSpot(0) shouldEqual ParkingLot(
      bottom = -1,
      top = 2,
      floors = Map(
        -1 -> Floor.empty(3),
        0  -> Floor.empty(3).takeSpot(),
        1  -> Floor.empty(3),
        2  -> Floor.empty(3),
      )
    )
  }

  test("release spot") {
    emptyParkingLot.takeSpot(0).releaseSpot(0) shouldEqual emptyParkingLot
  }

  test("Combustion vehicles allowed to all floors") {
    emptyParkingLot.isAllowed(Combustion, -1) shouldBe true
    emptyParkingLot.isAllowed(Combustion, 0) shouldBe true
    emptyParkingLot.isAllowed(Combustion, 1) shouldBe true
    emptyParkingLot.isAllowed(Combustion, 2) shouldBe true
  }

  test("Electric vehicles allowed to top two floors") {
    emptyParkingLot.isAllowed(Electric, -1) shouldBe false
    emptyParkingLot.isAllowed(Electric, 0) shouldBe false
    emptyParkingLot.isAllowed(Electric, 1) shouldBe true
    emptyParkingLot.isAllowed(Electric, 2) shouldBe true
  }

  test("Vans allowed to bottom two floors") {
    emptyParkingLot.isAllowed(Van, -1) shouldBe true
    emptyParkingLot.isAllowed(Van, 0) shouldBe true
    emptyParkingLot.isAllowed(Van, 1) shouldBe false
    emptyParkingLot.isAllowed(Van, 2) shouldBe false
  }

  test("find closest available spot") {
    val parkingLot = ParkingLot(
      bottom = -1,
      top = 3,
      floors = Map(
        -1 -> Floor.empty(1),
        0  -> Floor.empty(1),
        1  -> Floor.full(1),
        2  -> Floor.full(1),
        3  -> Floor.empty(1),
      )
    )

    parkingLot.findSpot(-1, Combustion) shouldEqual Some(-1)
    parkingLot.findSpot(0, Combustion) shouldEqual Some(0)
    parkingLot.findSpot(1, Combustion) shouldEqual Some(0)
    parkingLot.findSpot(2, Combustion) shouldEqual Some(3)
    parkingLot.findSpot(3, Combustion) shouldEqual Some(3)
  }
}
