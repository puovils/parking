package parking

import parking.core._
import org.scalatest._

class FloorSpec extends FunSuite with Matchers {

  test("create empty") {
    Floor.empty(3) shouldEqual Floor(3, 0)
  }

  test("decide when floor is fully occupied") {
    Floor(1, 0).notFull shouldEqual true
    Floor(1, 1).notFull shouldEqual false
  }

  test("take available spot") {
    Floor(1, 0).takeSpot() shouldEqual Floor(1, 1)
  }

  test("release occupied spot") {
    Floor(1, 1).releaseSpot() shouldEqual Floor(1, 0)
  }
}
