package parking

import cats.data.Chain
import cats.effect._
import cats.effect.concurrent.Ref
import cats.effect.test.TestConsole
import monix.execution.Scheduler.Implicits.global
import monix.reactive.Consumer
import org.scalatest._
import parking.core._

class ParkingLotAppSpec extends AppSuite {

  val emptyParkingLot = ParkingLotService.empty(bottom = -1, top = 2, floorCapacity = 3)
  val fullParkingLot  = ParkingLotService.empty(bottom = -1, top = 2, floorCapacity = 0)

  test("succefully enter and leave parking lot")(
    initialState = emptyParkingLot,
    input = Seq(
      "enter c1 2",
      "leave c1",
    ),
    output = Seq(
      "Drive to floor number 2.",
      "See you later, alligator!",
    )
  )

  test("there is no spots available")(
    initialState = fullParkingLot,
    input = Seq(
      "enter c1 2",
    ),
    output = Seq(
      "Sorry, no parking spots available right now. Try again later.",
    )
  )

  test("approaching is not found in registry")(
    initialState = emptyParkingLot,
    input = Seq(
      "enter foo 2",
    ),
    output = Seq(
      "Please register first, and then come back.",
    )
  )

  test("try to park already parked vehicle")(
    initialState = emptyParkingLot,
    input = Seq(
      "enter c1 2",
      "enter c1 1",
    ),
    output = Seq(
      "Drive to floor number 2.",
      "Vehicle with this plate number already parked.",
    )
  )

  test("try unpark vehicle that is not parked")(
    initialState = emptyParkingLot,
    input = Seq(
      "leave c1",
    ),
    output = Seq(
      "There is no vehicle parked with this plate number.",
    )
  )

  test("invalid command")(
    initialState = emptyParkingLot,
    input = Seq(
      "foo bar",
    ),
    output = Seq(
      "ERROR: Invalid command!",
    )
  )
}

trait AppSuite extends FunSuite with Matchers {

  case class Output(stdOut: Seq[String])

  def test(name: String)(input: Seq[String], output: Seq[String], initialState: ParkingLotService.State) =
    registerTest(name) {
      val test = for {
        read   <- TestConsole.inputs.sequenceAndDefault[IO](Chain.fromSeq(input), "exit")
        lines  <- Ref[IO].of(Chain.empty[String])
        words  <- Ref[IO].of(Chain.empty[String])
        errors <- Ref[IO].of(Chain.empty[String])
        console = TestConsole.make[IO](lines, words, errors, read)
        _   <- createApp(initialState)(console)
        out <- lines.get
        withoutPromt = out.toList.tail.filterNot(_ == ParkingLotApp.promt)
      } yield withoutPromt shouldEqual output

      test.unsafeRunSync()
    }

  private def createApp(initialState: ParkingLotService.State)(implicit console: Console[IO]) = {
    val service = new ParkingLotService(VehicleRegistry.dummy[IO])
    val app     = new ParkingLotApp[IO](service)
    app
      .create(initialState)
      .consumeWith(Consumer.complete)
      .toIO
  }
}
