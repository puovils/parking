package parking

import cats.Id
import parking.core._
import org.scalatest._

class ParkingLotServiceSpec extends FunSuite with Matchers {
  import Request._
  import Response._

  val service         = new ParkingLotService(VehicleRegistry.dummy[Id])
  val emptyParkingLot = ParkingLotService.empty(bottom = -1, top = 2, floorCapacity = 3)
  val fullParkingLot  = ParkingLotService.empty(bottom = -1, top = 2, floorCapacity = 0)

  test("already parked") {
    val state = emptyParkingLot.park("c1", 0)
    service.handleRequest(state, Enter("c1", 1)) shouldEqual AlreadyParked
  }

  test("successful parking") {
    service.handleRequest(emptyParkingLot, Enter("c1", 1)) shouldEqual Park("c1", 1)
  }

  test("no spots available") {
    service.handleRequest(fullParkingLot, Enter("c1", 1)) shouldEqual NoSpotsAvailable
  }

  test("unknown vehicle") {
    service.handleRequest(emptyParkingLot, Enter("foo", 1)) shouldEqual UnknownPlateNumber("foo")
  }

  test("successful unpark") {
    val state = emptyParkingLot.park("c1", 0)
    service.handleRequest(state, Leave("c1")) shouldEqual Unpark("c1")
  }

  test("unparking non-existing vehicle") {
    service.handleRequest(emptyParkingLot, Leave("c1")) shouldEqual VehicleNotFound
  }

  test("apply Park response") {
    service.applyResponse(emptyParkingLot, Park("c1", 0)) shouldEqual emptyParkingLot.park("c1", 0)
  }

  test("apply Unpark response") {
    val state = emptyParkingLot.park("c1", 0)
    service.applyResponse(state, Unpark("c1")) shouldEqual emptyParkingLot
  }

}
