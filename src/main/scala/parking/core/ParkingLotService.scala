package parking.core

import cats.Applicative
import cats.syntax.all._

class ParkingLotService[F[_]: Applicative](registry: VehicleRegistry[F]) {
  import ParkingLotService._
  import Request._
  import Response._

  def handleRequest(state: State, request: Request): F[Response] = request match {
    case Enter(plateNumber, barrier) =>
      if (state.isVehicleParked(plateNumber)) Applicative[F].pure(AlreadyParked)
      else
        registry(plateNumber).map {
          case Some(vehicleType) =>
            state.parkingLot.findSpot(barrier, vehicleType) match {
              case Some(level) => Park(plateNumber, level)
              case None        => NoSpotsAvailable
            }
          case None =>
            UnknownPlateNumber(plateNumber)
        }

    case Leave(plateNumber) =>
      Applicative[F].pure {
        if (state.isVehicleParked(plateNumber)) Unpark(plateNumber)
        else VehicleNotFound
      }
  }

  def applyResponse(state: State, response: Response): State = response match {
    case Park(plateNumber, level) => state.park(plateNumber, level)
    case Unpark(plateNumber)      => state.unpark(plateNumber)
    case _                        => state
  }
}

object ParkingLotService {
  case class State(parkingLot: ParkingLot, vehiclesParked: Map[PlateNumber, Level]) {
    def isVehicleParked(plateNumber: PlateNumber): Boolean = vehiclesParked.contains(plateNumber)

    def park(plateNumber: PlateNumber, level: Level): State = copy(
      parkingLot = parkingLot.takeSpot(level),
      vehiclesParked = vehiclesParked + (plateNumber -> level)
    )

    def unpark(plateNumber: PlateNumber): State = copy(
      parkingLot = parkingLot.releaseSpot(vehiclesParked(plateNumber)),
      vehiclesParked = vehiclesParked - plateNumber
    )
  }

  def empty(bottom: Level, top: Level, floorCapacity: Int): State = State(
    parkingLot = ParkingLot.empty(bottom, top, floorCapacity),
    vehiclesParked = Map.empty
  )
}

sealed trait Request
object Request {
  case class Enter(plateNumber: PlateNumber, barrier: Level) extends Request
  case class Leave(plateNumber: PlateNumber)                 extends Request
}

sealed trait Response
object Response {
  case class Park(plateNumber: PlateNumber, level: Level) extends Response
  case class Unpark(plateNumber: PlateNumber)             extends Response
  case class UnknownPlateNumber(plateNumber: PlateNumber) extends Response
  case object NoSpotsAvailable                            extends Response
  case object AlreadyParked                               extends Response
  case object VehicleNotFound                             extends Response
}
