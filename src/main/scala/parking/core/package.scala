package parking

package object core {
  type PlateNumber = String
  type Level       = Int
}
