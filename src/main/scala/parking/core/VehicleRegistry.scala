package parking.core

import cats.Applicative

trait VehicleRegistry[F[_]] {
  def apply(plateNumber: PlateNumber): F[Option[VehicleType]]
}

object VehicleRegistry {
  import VehicleType._

  def constant[F[_]: Applicative](registry: (PlateNumber, VehicleType)*): VehicleRegistry[F] = {
    val byPlateNumber = registry.toMap
    plateNumber =>
      Applicative[F].pure(byPlateNumber.get(plateNumber))
  }

  def dummy[F[_]: Applicative]: VehicleRegistry[F] =
    constant(
      "c1" -> Combustion,
      "c2" -> Combustion,
      "c3" -> Combustion,
      "e1" -> Electric,
      "e2" -> Electric,
      "e3" -> Electric,
      "v1" -> Van,
      "v2" -> Van,
      "v3" -> Van,
    )
}
