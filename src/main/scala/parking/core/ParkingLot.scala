package parking.core

import parking.core.VehicleType._

case class ParkingLot(bottom: Level, top: Level, floors: Map[Level, Floor]) {
  def takeSpot(level: Level)    = copy(floors = floors + (level -> floors(level).takeSpot()))
  def releaseSpot(level: Level) = copy(floors = floors + (level -> floors(level).releaseSpot()))

  def findSpot(barrier: Level, vehicleType: VehicleType): Option[Level] = {
    val available = floors.collect {
      case (level, floor) if floor.notFull && isAllowed(vehicleType, level) => level
    }
    if (available.isEmpty) None
    else Some(available.minBy(_.distanceTo(barrier)))
  }

  def isAllowed(vehicleType: VehicleType, level: Level): Boolean =
    vehicleType match {
      case Electric if level.distanceTo(top) <= 1 => true
      case Van if level.distanceTo(bottom) <= 1   => true
      case Combustion                             => true
      case _                                      => false
    }

  private implicit class DistanceBetweenLevels(level: Level) {
    def distanceTo(other: Level) = Math.abs(level - other)
  }
}

object ParkingLot {
  def empty(bottom: Level, top: Level, floorCapacity: Int): ParkingLot = {
    val levels = bottom to top
    val floors = Stream.continually(Floor.empty(floorCapacity))
    ParkingLot(
      bottom,
      top,
      floors = levels.zip(floors).toMap
    )
  }
}

case class Floor(capacity: Int, occupancy: Int) {
  def notFull       = capacity > occupancy
  def takeSpot()    = copy(occupancy = occupancy + 1)
  def releaseSpot() = copy(occupancy = occupancy - 1)
}

object Floor {
  def empty(capacity: Int) = Floor(capacity, 0)
  def full(capacity: Int)  = Floor(capacity, capacity)
}

sealed trait VehicleType
object VehicleType {
  case object Combustion extends VehicleType
  case object Electric   extends VehicleType
  case object Van        extends VehicleType
}
