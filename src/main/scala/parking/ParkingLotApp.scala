package parking

import cats.MonadError
import cats.effect.Console
import cats.syntax.all._
import monix.eval.TaskLike
import monix.reactive.Observable
import parking.core._

class ParkingLotApp[F[_]: TaskLike: Console](service: ParkingLotService[F])(implicit M: MonadError[F, Throwable]) {
  import ParkingLotService._
  import Command._
  import Response._

  def create(initialState: State): Observable[Unit] = {
    val intro = Observable.fromTaskLike(
      Console[F].putStrLn(
        """
        | enter <plate number> <floor>
        |   Indicates approaching vehicle with <plate number> at boom barrier that is placed in <floor>.
        |   Examples: 
        |     enter c1 2
        |     enter e2 -1
        |     
        | leave <plate number>
        |   Indicates that vehicle with <plate nuber> is leaving parking lot.
        |   Examples:
        |     leave c1
        |     leave e2
        |   
        | exit
        |   Quit parking application
      """.stripMargin))

    val loop = Observable
      .repeatEvalF(readCommand)
      .takeWhile(_ != Exit)
      .doOnErrorF(e => Console[F].putStrLn(s"ERROR: ${e.getMessage}"))
      .onErrorRestartUnlimited
      .collect { case ParkingRequest(r) => r }
      .scanEvalF(M.pure(initialState))(applyRequest)

    for {
      _ <- intro
      _ <- loop
    } yield ()
  }

  private def applyRequest(state: State, request: Request): F[State] =
    for {
      response <- service.handleRequest(state, request)
      _        <- Console[F].putStrLn(message(response))
    } yield service.applyResponse(state, response)

  private def readCommand: F[Command] =
    for {
      _       <- Console[F].putStr(ParkingLotApp.promt)
      input   <- Console[F].readLn
      command <- parseCommand(input)
    } yield command

  def message(response: Response): String = response match {
    case Park(_, level)        => s"Drive to floor number $level."
    case Unpark(_)             => "See you later, alligator!"
    case NoSpotsAvailable      => "Sorry, no parking spots available right now. Try again later."
    case UnknownPlateNumber(_) => "Please register first, and then come back."
    case AlreadyParked         => "Vehicle with this plate number already parked."
    case VehicleNotFound       => "There is no vehicle parked with this plate number."
  }

  private def parseCommand(input: String): F[Command] = input match {
    case Command(c) => M.pure(c)
    case _          => M.raiseError(new RuntimeException("Invalid command!"))
  }
}

object ParkingLotApp {
  val promt = "parking> "
}

sealed trait Command
object Command {
  case class ParkingRequest(request: Request) extends Command
  case object Exit                            extends Command

  def unapply(input: String): Option[Command] = input match {
    case "exit"          => Some(Exit)
    case MatchRequest(r) => Some(ParkingRequest(r))
    case _               => None
  }
}

object MatchRequest {
  import Request._

  private val enter = """enter (\w+) ([+-]?\d+)""".r
  private val leave = """leave (\w+)""".r

  def unapply(input: String): Option[Request] = input match {
    case leave(plateNumber)          => Some(Leave(plateNumber))
    case enter(plateNumber, barrier) => Some(Enter(plateNumber, barrier.toInt))
    case _                           => None
  }
}
