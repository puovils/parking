package parking

import cats.effect.Console.implicits._
import cats.effect._
import cats.syntax.all._
import monix.execution.Scheduler.Implicits.global
import monix.reactive.Consumer
import parking.core._

object Main extends IOApp {
  val service = new ParkingLotService(VehicleRegistry.dummy[IO])
  val app     = new ParkingLotApp(service)

  def run(args: List[String]): IO[ExitCode] =
    app
      .create(ParkingLotService.empty(bottom = -1, top = 2, floorCapacity = 3))
      .consumeWith(Consumer.complete)
      .as(ExitCode.Success)
      .toIO
}
